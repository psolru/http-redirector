FROM nginx:1.21.0-alpine

RUN apk add bash --no-cache

COPY etc/nginx/conf.d/default.conf /etc/nginx/conf.d/default.conf

COPY etc/nginx/conf.d/rewrite.template .

COPY run.sh .

CMD ["bash", "run.sh"]
