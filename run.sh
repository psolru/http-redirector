#!/bin/bash

IFS=',' read -r -a NGINX_REWRITES <<< "${NGINX_REWRITES}"

echo "Creating rewrite rules..."

for REWRITE_RULE in "${NGINX_REWRITES[@]}"
do
  IFS=':' read -r ORIGIN REWRITE_DESTINATION <<< "${REWRITE_RULE}"

  echo "${ORIGIN} => ${REWRITE_DESTINATION}"

  DESTINATION_FILE="/etc/nginx/conf.d/${ORIGIN}.conf"

  cp rewrite.template "${DESTINATION_FILE}"

  sed -i -e 's,NGINX_ORIGIN_URL,'"${ORIGIN}"',g' "${DESTINATION_FILE}"
  sed -i -e 's,NGINX_DESTINATION_URL,'"${REWRITE_DESTINATION}"',g' "${DESTINATION_FILE}"

done

echo "Starting nginx..."

exec nginx -g "daemon off;"
