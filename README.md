# HTTP Redirector
[![Docker Pulls](https://img.shields.io/docker/pulls/psolru/http-redirector)](https://hub.docker.com/r/psolru/http-redirector)
[!["Buy Me A Coffee"](https://www.buymeacoffee.com/assets/img/custom_images/orange_img.png)](https://www.buymeacoffee.com/psolru)

Redirects http requests to specific destination address. Configurable via ENV variables.

## Examples

Docker cli
```bash
docker run --rm --name http-redirector \
  -p 8080:80 \
  -e NGINX_REWRITES=example.com:https://discord.gg/example,foobar.com:https://discord.gg/foobar,test-abc.com:https://discord.gg/test-abc \
  psolru/http-redirector
```

Docker Compose
```yml
---
version: "3.0"
services:
  web:
    image: psolru/http-redirector
    ports:
      - "8080:80"
    environment:
      - NGINX_REWRITES=example.com:https://discord.gg/example,foobar.com:https://discord.gg/foobar,test-abc.com:https://discord.gg/test-abc

```

## Testing you redirector
Example 
```bash
$ curl -IH "Host: example.com" localhost:8080
HTTP/1.1 301 Moved Permanently
Server: nginx/1.21.0
Date: Fri, 05 Nov 2021 22:31:03 GMT
Content-Type: text/html
Content-Length: 169
Connection: keep-alive
Location: https://discord.gg/example
```
